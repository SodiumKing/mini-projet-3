try:
        import csv
        import sqlite3 
        import logging
except Exception as e:
        log = "Problème de librairie"
        logging.error(log)
        print(log)


connexion = sqlite3.connect('baseprojet3.db')
logging.basicConfig(filename='logging.log')


curseur = connexion.cursor()

curseur.execute('''CREATE TABLE IF NOT EXISTS automobile (adresse_titulaire TEXT,
            nom TEXT,
            prenom TEXT,
            immatriculation TEXT,
            date_immatriculation TEXT,
            vin INTEGER,
            marque TEXT,
            denomination_commerciale TEXT,
            couleur TEXT,
            carrosserie TEXT,
            categorie TEXT,
            cylindree INTEGER,
            energie INTEGER,
            places INTEGER,
            poids INTEGER,
            puissance INTEGER,
            type TEXT,
            variante TEXT,
            version INTEGER);''')
logging.info('Création de la table')
           


def Insertion():
        try:
                with open('auto.csv') as csvfile:
                        data = csv.DictReader(csvfile,delimiter = ';',quotechar= ' ')
                        vers_DB = [(i['adresse_titulaire'],i['nom'],i['prenom'],i['immatriculation'],i['date_immatriculation'],i['vin'],i['marque'],i['denomination_commerciale'],i['couleur'],i['carrosserie'],i['categorie'],i['cylindree'],i['energie'],i['places'],i['poids'],i['puissance'],i['type'],i['variante'],i['version'])for i in data]
                        curseur.executemany('''INSERT INTO automobile (adresse_titulaire,
                                nom ,prenom,immatriculation,date_immatriculation,
                                vin,marque,denomination_commerciale,couleur,carrosserie,
                                categorie,cylindree,energie,places,poids,puissance,type,
                                variante,version)VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);''',vers_DB)

        except Exception as e:
                log = "Une erreur est survenue dans Insertion"
                logging.error(log)
                print(log)
        connexion.commit()



def Update():
        try:
                query = '''UPDATE automobile SET adresse_titulaire = ?, nom = ?, prenom = ?, immatriculation = ?, date_immatriculation = ?,
                    vin = ?, marque = ?, denomination_commerciale = ?, couleur = ?, carrosserie = ?, categorie = ?, cylindree = ?,
                    energie = ?, places = ?, poids = ?, puissance = ?, type = ?, variante = ?, version = ? WHERE id = ?'''
        except Exception as e:
                log = "Une erreur est survenue lors de l'Update: " + e
                logging.error(log)
                print(log) 
        curseur.execute(query)

        connexion.commit()



Insertion()

connexion.close()

log= "fin du programme"
logging.info(log)
